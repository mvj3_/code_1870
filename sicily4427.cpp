//题目分析:水题不分析

//题目网址:http://soj.me/4427

#include <iostream>

using namespace std;

int cases;
int a, b;
int low, high;

int main()
{
    int i;
    cin >> cases;
    while (cases--) {
        cin >> a >> b >> low >> high;
        
        for (i = high; i >= low; i--) {
            if (a % i == 0 && b % i == 0) {
                break;
            }
        }
        if (i >= low) {
            cout << i << endl;
        } else {
            cout << "No answer" << endl;
        }
    }
    
    return 0;
}                                 